FROM debian:unstable-slim

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    mkdir /usr/share/man/man1 && \
    apt-get install -y --no-install-recommends openjdk-13-jre-headless && \
    apt-get autoremove --purge -y && \
    rm -rv /var/lib/apt/lists/* && \
    useradd --no-log-init --shell /bin/false --no-create-home wikidevbot

ADD wikidevbot.tar /opt/
RUN chown -R wikidevbot:wikidevbot /opt/wikidevbot/

USER wikidevbot
WORKDIR /opt/wikidevbot/

ENTRYPOINT ["bin/wikidevbot"]
