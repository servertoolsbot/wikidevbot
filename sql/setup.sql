CREATE TABLE settings(
    setting varchar(255),
    value varchar(255)
);
INSERT INTO settings (setting, value)
VALUES
    ( 'version', '2' ),
    ( 'url', 'https://phabricator.wikimedia.org/' ),
    ( 'gerriturl', 'https://gerrit.wikimedia.org/r/' ),
    ( 'gerritchangeurl', 'https://gerrit.wikimedia.org/r/c/$%' ),
    ( 'api-token', '--CONDUIT API TOKEN--' ),
    ( 'gerrit-user', '--YOUR GERRIT USER HERE--' ),
    ( 'gerrit-password', '--YOUR GERRIT PASSWORD HERE--');

CREATE TABLE messages(
    id varchar(255),
    value varchar(255),
    locale varchar(255)
);

CREATE TABLE server_locales(
    server_id varchar(255),
    locale_id varchar(255)
);
