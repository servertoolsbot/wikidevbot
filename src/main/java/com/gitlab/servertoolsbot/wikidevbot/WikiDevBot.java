package com.gitlab.servertoolsbot.wikidevbot;

import com.gitlab.servertoolsbot.util.commandmanager.CommandManager;
import com.gitlab.servertoolsbot.util.messagemanager.Message;
import com.gitlab.servertoolsbot.util.messagemanager.MessageManager;
import com.gitlab.servertoolsbot.util.messagemanager.MessageStorageType;
import com.gitlab.servertoolsbot.util.permissionmanager.PermissionManager;
import com.gitlab.servertoolsbot.util.permissionmanager.configuration.PermissionStorageType;
import com.gitlab.servertoolsbot.util.phabricatorapi.PhabricatorApi;
import com.gitlab.servertoolsbot.wikidevbot.commands.HelpCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.ProjectCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.TaskCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.UserCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.gerrit.ChangeById;
import com.gitlab.servertoolsbot.wikidevbot.commands.permissions.DoIHasPermissionCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.permissions.groups.AddGroupCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.permissions.groups.DeleteGroupCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.permissions.groups.ListGroupsCommand;
import com.gitlab.servertoolsbot.wikidevbot.commands.permissions.users.ListUsersCommand;
import com.gitlab.servertoolsbot.wikidevbot.util.util.DatabaseUtil;
import com.google.gerrit.extensions.api.GerritApi;
import com.urswolfer.gerrit.client.rest.GerritAuthData;
import com.urswolfer.gerrit.client.rest.GerritRestApiFactory;
import io.prometheus.client.Counter;
import io.prometheus.client.exporter.HTTPServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.permission.Permissions;
import org.javacord.api.event.message.MessageCreateEvent;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class WikiDevBot {
    private static Logger logger = LogManager.getLogger(WikiDevBot.class);

    private static WikiDevBot instance;
    public static Locale DEFAULT_LOCALE = Locale.ENGLISH;
    private DiscordApi api;
    private MessageCreateEvent event;
    private CommandManager commandManager = new CommandManager();
    private MessageManager messageManager = new MessageManager();
    private PermissionManager permissionManager = new PermissionManager();
    private GerritApi gapi = null;
    private String gerritUrl = null;
    private String gerritChangeUrl = null;
    private PhabricatorApi papi = null;
    private Connection dbConnection = null;
    private boolean activityScheduler = false;

    // Prometheus
    private static final Counter commandExecutions = Counter.build().name("st_wikidevbot_command_executions_total").help("Total command executions.").register();

    private void setActivityScheduler(boolean activityScheduler) {
        this.activityScheduler = activityScheduler;
    }
    private boolean getActivityScheduler() {
        return this.activityScheduler;
    }

    public static WikiDevBot getInstance() {
        return instance;
    }
    public DiscordApi getApi() {
        return this.api;
    }
    public CommandManager getCommandManager() {
        return this.commandManager;
    }
    public MessageManager getMessageManager() {
        return this.messageManager;
    }
    public PermissionManager getPermissionManager() {
        return this.permissionManager;
    }
    public MessageCreateEvent getEvent() {
        return this.event;
    }
    public GerritApi getGapi() {
        return this.gapi;
    }
    public String getGerritUrl() {
        return this.gerritUrl;
    }
    public String getGerritChangeUrl() {
        return this.gerritChangeUrl;
    }
    public PhabricatorApi getPapi() {
        return this.papi;
    }
    public Connection getDbConnection() {
        return this.dbConnection;
    }

    public void start(String[] args) {
        instance = this;
        api = new DiscordApiBuilder().setToken(args[0]).login().join();
        try {
            dbConnection = DriverManager.getConnection("jdbc:mariadb://"
                    + args[3] + ":3306/" + args[4]
                    + "?user=" + args[1] + "&password=" + args[2]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseUtil dbUtil = new DatabaseUtil(this.dbConnection);
        papi = new PhabricatorApi(
                dbUtil.getFromTable("settings", "setting", "url", "value"),
                dbUtil.getFromTable("settings", "setting", "api-token", "value")
                );
        gerritUrl = dbUtil.getFromTable("settings", "setting", "gerriturl", "value");
        gapi = new GerritRestApiFactory().create(new GerritAuthData.Basic(
                gerritUrl,
                dbUtil.getFromTable("settings", "setting", "gerrituser", "value"),
                dbUtil.getFromTable("settings", "setting", "gerritpassword", "value")
        ));
        gerritChangeUrl = dbUtil.getFromTable("settings", "setting", "gerritchangeurl", "value");

        api.getThreadPool().getScheduler().scheduleAtFixedRate(() -> {
            if (getActivityScheduler()) {
                api.updateActivity(ActivityType.WATCHING, gerritUrl);
                setActivityScheduler(false);
            } else {
                api.updateActivity(ActivityType.WATCHING, papi.getUrlRaw());
                setActivityScheduler(true);
            }
        }, 0L, 60L, TimeUnit.SECONDS);
        logger.info("Invite the bot using: " + api.createBotInvite(Permissions.fromBitmask(0)));

        setupPrometheus();
        
        registerCommands();
        registerMessages(args);

        setupListeners();
    }

    private void registerMessages(String[] args) {
        MessageStorageType type = MessageStorageType.MARIADB;
        type.setUser(args[1]);
        type.setPassword(args[2]);
        type.setDatabase(args[4]);
        type.setTable("messages");
        type.setHost(args[3]);

        messageManager.setStorageType(type);
        messageManager.setDefaultLocale(Locale.ENGLISH);

        messageManager.addMessages(
                new Message("command.user.title", "User %{username} (%{realname})").inLocale(Locale.ENGLISH)
                    .addVariable("username", "not found")
                    .addVariable("realname", "empty"),
                new Message("command.user.roles", "Roles").inLocale(Locale.ENGLISH),
                new Message("common.description", "Description").inLocale(Locale.ENGLISH),
                new Message("common.created", "Created").inLocale(Locale.ENGLISH),
                new Message("common.modified", "Modified").inLocale(Locale.ENGLISH),
                new Message("common.no", "No").inLocale(Locale.ENGLISH),
                new Message("common.yes", "Yes").inLocale(Locale.ENGLISH),
                new Message("gerrit.owner", "Owner").inLocale(Locale.ENGLISH),
                new Message("gerrit.assignee", "Assignee").inLocale(Locale.ENGLISH),
                new Message("gerrit.reviewers", "Reviewers").inLocale(Locale.ENGLISH),
                new Message("help.menu", "Help menu").inLocale(Locale.ENGLISH),
                new Message("task.closed", "Closed").inLocale(Locale.ENGLISH),
                new Message("task.priority", "Priority").inLocale(Locale.ENGLISH),
                new Message("task.status", "Status").inLocale(Locale.ENGLISH),
                new Message("task.subscribers", "Subscribers").inLocale(Locale.ENGLISH),
                new Message("project.watcher.singular", "%{count} Watcher").inLocale(Locale.ENGLISH)
                    .addVariable("count", "1"),
                new Message("project.watcher.plural", "%{count} Watchers").inLocale(Locale.ENGLISH)
                    .addVariable("count", "Multiple"),
                new Message("project.member.singular", "%{count} Member").inLocale(Locale.ENGLISH)
                    .addVariable("count", "1"),
                new Message("project.member.plural", "%{count} Members").inLocale(Locale.ENGLISH)
                    .addVariable("count", "Multiple"),

                new Message("command.user.title", "Benutzer %{username} (%{realname})").inLocale(Locale.GERMAN)
                    .addVariable("username", "nicht gefunden")
                    .addVariable("realname", "leer"),
                new Message("command.user.roles", "Rollen").inLocale(Locale.GERMAN),
                new Message("common.description", "Beschreibung").inLocale(Locale.GERMAN),
                new Message("common.created", "Erstellt").inLocale(Locale.GERMAN),
                new Message("common.modified", "Ge\u00E4ndert").inLocale(Locale.GERMAN),
                new Message("common.no", "Nein").inLocale(Locale.GERMAN),
                new Message("common.yes",  "Ja").inLocale(Locale.GERMAN),
                new Message("gerrit.owner", "Besitzer").inLocale(Locale.GERMAN),
                new Message("gerrit.assignee", "Zugewiesene/r").inLocale(Locale.GERMAN),
                new Message("gerrit.reviewers", "\u00DCberpr\u00FCfer/innen").inLocale(Locale.GERMAN),
                new Message("help.menu", "Hilfemen\u00FC").inLocale(Locale.GERMAN),
                new Message("task.closed", "Geschlossen").inLocale(Locale.GERMAN),
                new Message("task.priority", "Priorit\u00E4t").inLocale(Locale.GERMAN),
                new Message("task.status", "Status").inLocale(Locale.GERMAN),
                new Message("task.subscribers", "Teilnemer/innen").inLocale(Locale.GERMAN),
                new Message("project.watcher.singular", "%{count} Beobachter").inLocale(Locale.GERMAN)
                        .addVariable("count", "1"),
                new Message("project.watcher.plural", "%{count} Beobachter").inLocale(Locale.GERMAN)
                        .addVariable("count", "Mehrere"),
                new Message("project.member.singular", "%{count} Mitglied").inLocale(Locale.GERMAN)
                        .addVariable("count", "1"),
                new Message("project.member.plural", "%{count} Mitglieder").inLocale(Locale.GERMAN)
                        .addVariable("count", "Mehrere")

        );

        messageManager.load();
    }
    private void setupPermissionManager(String[] args) {
        PermissionStorageType type = PermissionStorageType.MARIADB;
        type.setUser(args[1]);
        type.setPassword(args[2]);
        type.setDatabase(args[4]);
        type.setHost(args[3]);

        // Tables
        type.setUserTable("users");
        type.setUserParentTable("user_parents");
        type.setUserPermissionTable("user_permissions");
        type.setGroupTable("groups");
        type.setGroupParentTable("group_parents");
        type.setGroupPermissionTable("group_permissions");

        permissionManager.setStorageType(type);
        permissionManager.start();
    }
    private void registerCommands() {
        commandManager.setPrefix("-");

        commandManager.registerCommand(HelpCommand.class);

        // Permissions
        commandManager.registerCommand(ListGroupsCommand.class);
        commandManager.registerCommand(AddGroupCommand.class);
        commandManager.registerCommand(ListUsersCommand.class);
        commandManager.registerCommand(DoIHasPermissionCommand.class);
        commandManager.registerCommand(DeleteGroupCommand.class);

        // Phabricator
        commandManager.registerCommand(UserCommand.class);
        commandManager.registerCommand(TaskCommand.class);
        commandManager.registerCommand(ProjectCommand.class);

        // Gerrit
        commandManager.registerCommand(ChangeById.class);
    }
    private void setupListeners() {
        api.addMessageCreateListener(event -> {
            if (!event.getMessageAuthor().isYourself()) {
                this.event = event;
                commandManager.handleInput(event.getMessageContent());
            }
        });
    }
    private void setupPrometheus()  {
        try {
            HTTPServer server = new HTTPServer(9091);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void incrementCommandExecutions() {
        commandExecutions.inc();
    }
}
