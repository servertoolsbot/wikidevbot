package com.gitlab.servertoolsbot.wikidevbot.util.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseUtil {
    private Connection dbConnection;

    public DatabaseUtil(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public String getFromTable(String table, String row, String rowValue, String rowToReadFrom) {
        return getFromQuery("SELECT * FROM `" + table + "` WHERE " + row + "='" + rowValue + "';", rowToReadFrom);
    }

    public String getFromQuery(String query, String rowToReadFrom) {
        String result = "";
        try {
            ResultSet resultSet = dbConnection.createStatement().executeQuery(query);
            while (resultSet.next()) result = resultSet.getString(rowToReadFrom);
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
