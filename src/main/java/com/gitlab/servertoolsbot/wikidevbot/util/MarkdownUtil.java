package com.gitlab.servertoolsbot.wikidevbot.util;

public class MarkdownUtil {
    public static void main(String[] args) {
        System.out.println(formatLinks("Defines a collection of data types ([[ https://github.com/wmde/DataTypes | Homepage ]])"));
    }

    public static String formatAll(String toFormat) {
        toFormat = formatLinks(toFormat);

        System.out.println(toFormat);
        return toFormat;
    }

    public static String formatLinks(String toFormat) {
        return toFormat.replaceAll("(\\[\\[\\s(.*)\\s\\|\\s(.*)\\s\\]\\])", "[$3]($2)");
    }
}
