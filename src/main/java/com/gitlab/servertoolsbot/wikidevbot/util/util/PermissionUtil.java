package com.gitlab.servertoolsbot.wikidevbot.util.util;

import com.gitlab.servertoolsbot.util.permissionmanager.GroupManager;
import com.gitlab.servertoolsbot.util.permissionmanager.PermissionManager;
import com.gitlab.servertoolsbot.util.permissionmanager.UserManager;

public class PermissionUtil {
    private PermissionManager manager;
    private UserManager um;
    private GroupManager gm;

    public PermissionUtil(PermissionManager manager) {
        this.manager = manager;
        this.um = manager.getUserManager();
        this.gm = manager.getGroupManager();
    }

    public boolean performPermissionCheck(String id, String permission) {
        return userExists(id) && um.getUser(id).hasPermission(permission);
    }

    public boolean userExists(String name) {
        return um.getAllUsers().stream().anyMatch(user -> {
            return user.getName().equals(name);
        });
    }
    public boolean groupExists(String name) {
        return gm.getAllGroups().stream().anyMatch(group -> {
            return group.getName().equals(name);
        });
    }
}
