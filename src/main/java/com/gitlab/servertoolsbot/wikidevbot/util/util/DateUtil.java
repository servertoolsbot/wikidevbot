package com.gitlab.servertoolsbot.wikidevbot.util.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
    public static SimpleDateFormat getDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd MMM yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf;
    }

    public static String formatDate(int date) {
        return formatDate(getDateFormat(), date);
    }
    public static String formatDate(SimpleDateFormat sdf, int date) {
        return sdf.format(new Date(Long.valueOf(String.valueOf(date))*1000L));
    }
}
