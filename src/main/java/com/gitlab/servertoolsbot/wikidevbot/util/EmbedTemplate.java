package com.gitlab.servertoolsbot.wikidevbot.util;

import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.awt.*;

public class EmbedTemplate {
    public EmbedBuilder red() {
        return colored(Color.RED);
    }
    public EmbedBuilder green() {
        return colored(Color.GREEN);
    }
    public EmbedBuilder gray() {
        return colored(Color.GRAY);
    }

    public EmbedBuilder colored(Color color) {
        return common().setColor(color);
    }
    public EmbedBuilder common() {
        return new EmbedBuilder();
    }
}
