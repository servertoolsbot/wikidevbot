package com.gitlab.servertoolsbot.wikidevbot.util.util;

public class MarkdownUtil {
    public static String formatAll(String toFormat) {
        toFormat = formatLinks(toFormat);

        return toFormat;
    }

    public static String formatLinks(String toFormat) {
        return toFormat.replaceAll("\\[\\[\\s([^\\|]+)\\s\\|\\s([^\\]]+)\\s\\]\\]", "[$2]($1)");
    }
}
