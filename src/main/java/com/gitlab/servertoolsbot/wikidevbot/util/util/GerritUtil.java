package com.gitlab.servertoolsbot.wikidevbot.util.util;

import com.gitlab.servertoolsbot.wikidevbot.WikiDevBot;
import com.google.gerrit.extensions.common.AccountInfo;

public class GerritUtil {
    public static String buildGerritUserLink(AccountInfo account) {
        return "[" + account.name + "](" + WikiDevBot.getInstance().getGerritUrl() + "q/owner:" + account.email + ")";
    }
}
