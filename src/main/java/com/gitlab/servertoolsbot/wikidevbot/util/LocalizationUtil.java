package com.gitlab.servertoolsbot.wikidevbot.util;

import com.gitlab.servertoolsbot.wikidevbot.WikiDevBot;

import java.util.Locale;

public class LocalizationUtil {
    public static Locale getLocaleForServer(String id) {
        DatabaseUtil util = new DatabaseUtil(WikiDevBot.getInstance().getDbConnection());

        String result = util.getFromTable("server_locales", "server_id", id, "locale_id");
        if (result == null || result.equals("")) {
            return WikiDevBot.DEFAULT_LOCALE;
        } else {
            return new Locale(result);
        }
    }
}
