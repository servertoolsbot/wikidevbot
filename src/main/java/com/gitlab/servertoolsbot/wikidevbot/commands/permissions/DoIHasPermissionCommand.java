package com.gitlab.servertoolsbot.wikidevbot.commands.permissions;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;

@Command(name = "doihaspermission", description = "Check if you have a permission", usage = "<permission>", assertPermission = "command.doihaspermission.execute")
public class DoIHasPermissionCommand extends CommonCommand {

    @Override
    @CommandParam(0)
    public void noArgumentsProvided() {
        channel.sendMessage("Please give a permission node to check");
    }

    @CommandParam(1)
    public void check() {
        channel.sendMessage(String.valueOf(permissionUtil.performPermissionCheck(author.getIdAsString(), param.get(1))));
    }
}
