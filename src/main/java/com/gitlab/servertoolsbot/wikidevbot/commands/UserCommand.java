package com.gitlab.servertoolsbot.wikidevbot.commands;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.util.messagemanager.MessageReplacement;
import com.gitlab.servertoolsbot.util.phabricatorapi.PhabricatorApi;
import com.gitlab.servertoolsbot.util.phabricatorapi.data.model.User;
import com.gitlab.servertoolsbot.util.phabricatorapi.data.model.UserSearch;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;
import com.gitlab.servertoolsbot.wikidevbot.util.util.DateUtil;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

@Command(name = "user", usage = "<name>", description = "Shows information about a user")
public class UserCommand extends CommonCommand {
    @Override
    @CommandParam(1)
    public void noArgumentsProvided() {
        UserSearch search = new UserSearch();
        search.setConstraints(new HashMap<String, Object>() {
            {
                put("usernames", Arrays.asList(param.get(1)));
            }
        });
        search.setLimit(1);

        User u = search.perform();

        EmbedBuilder em = embed.common();

        em.setTitle(messageManager.getParsedMessage("command.user.title",
                new MessageReplacement("username", u.username),
                new MessageReplacement("realname", u.realName)));
        em.addField(messageManager.getMessage("command.user.roles"), String.join(", ", u.roles.stream().map(this::uppercaseFirstLetter).collect(Collectors.toList())));

        em.addField(messageManager.getMessage("common.created"), DateUtil.formatDate(u.dateCreated), true);
        em.addField(messageManager.getMessage("common.modified"), DateUtil.formatDate(u.dateModified), true);

        channel.sendMessage(PhabricatorApi.getPapi().getUrlRaw() + "p/" + u.username, em);
    }

    private String uppercaseFirstLetter(String to) {
        return to.replaceFirst(to.substring(0, 1), to.substring(0, 1).toUpperCase());
    }
}
