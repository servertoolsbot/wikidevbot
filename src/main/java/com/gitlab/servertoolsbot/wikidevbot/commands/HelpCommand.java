package com.gitlab.servertoolsbot.wikidevbot.commands;

import com.gitlab.servertoolsbot.util.commandmanager.BasicCommand;
import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandManager;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.wikidevbot.WikiDevBot;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.util.ArrayList;
import java.util.List;

@Command(name = "help", description = "Shows a help menu", usage = "")
public class HelpCommand extends CommonCommand {
    @Override
    @CommandParam(0)
    public void noArgumentsProvided() {
        EmbedBuilder em = embed.gray();
        em.setTitle("Help menu");
        CommandManager manager = WikiDevBot.getInstance().getCommandManager();
        List<String> commands = new ArrayList<>();
        manager.getRegisteredCommands().forEach(clazz -> {
            try {
                BasicCommand basic = clazz.newInstance();
                String name = basic.getCommandName();
                String desc = basic.getCommandDescription();
                String usage = basic.getCommandUsage();
                String assertPermission = basic.getCommandAssertPermission();
                commands.add("`" + manager.getPrefix() + name + (usage != null ? " " + usage : "") + "`: " + desc + (assertPermission.equals("") ? "" : "\n**Permission:** `" + assertPermission + "`\n"));
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        em.setDescription(String.join("\n", commands));

        channel.sendMessage(em);
    }
}
