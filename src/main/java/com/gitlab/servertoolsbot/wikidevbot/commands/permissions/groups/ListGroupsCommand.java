package com.gitlab.servertoolsbot.wikidevbot.commands.permissions.groups;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.util.permissionmanager.Group;
import com.gitlab.servertoolsbot.util.permissionmanager.User;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;

import java.util.stream.Collectors;

@Command(name = "listgroups", usage = "[<user | group>] [<username | groupname>]", aliases = { "groups" }, description = "Lists all groups in the database", assertPermission = "addgroups.command.execute")
public class ListGroupsCommand extends CommonCommand {

    @Override
    @CommandParam(0)
    public void noArgumentsProvided() {
        channel.sendMessage("**These are all available groups:**\n`" + permissionManager.getGroupManager().getAllGroups().stream().map(Group::getName).collect(Collectors.joining("`, `")) + "`");
    }

    @CommandParam(1)
    public void ok() {
        channel.sendMessage("Please give a group or user name now.");
    }

    @CommandParam(2)
    public void ok2() {
        if (!(param.get(1).equalsIgnoreCase("group") || param.get(1).equalsIgnoreCase("user"))) {
            channel.sendMessage("Argument 2 was not \"group\" or \"user\"");
            return;
        }
        if (param.get(1).equalsIgnoreCase("group")) {
            if (permissionManager.getGroupManager().getAllGroups().stream().map(Group::getName).collect(Collectors.toList()).contains(param.get(2))) {
                channel.sendMessage("Parents of " + param.get(2) + ":\n" + permissionManager.getGroupManager().getGroup(param.get(2)).getParents().stream().map(Group::getName).collect(Collectors.joining(", ")));
            } else {
                channel.sendMessage("Group doesn't exists.");
            }
        }
        if (param.get(1).equalsIgnoreCase("user")) {
            if (permissionManager.getUserManager().getAllUsers().stream().map(User::getName).collect(Collectors.toList()).contains(param.get(2))) {
                channel.sendMessage("Parents of " + param.get(2) + ":\n" + permissionManager.getUserManager().getUser(param.get(2)).getParents().stream().map(Group::getName).collect(Collectors.joining(", ")));
            } else {
                channel.sendMessage("User doesn't exists");
            }
        }
    }
}
