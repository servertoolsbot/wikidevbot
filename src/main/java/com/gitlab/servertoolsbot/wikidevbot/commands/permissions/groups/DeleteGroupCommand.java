package com.gitlab.servertoolsbot.wikidevbot.commands.permissions.groups;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.util.permissionmanager.Group;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;

import java.util.stream.Collectors;

@Command(name = "deletegroup", usage = "<name>", description = "Remove a group from the database.", assertPermission = "command.deletegroups.execute")
public class DeleteGroupCommand extends CommonCommand {

    @Override
    @CommandParam(0)
    public void noArgumentsProvided() {
        channel.sendMessage("Sorry, but please give a group name");
    }

    @CommandParam(1)
    public void groupProvided() {
        if (!permissionManager.getGroupManager().getAllGroups().stream().map(Group::getName).collect(Collectors.toList()).contains(param.get(1))) {
            channel.sendMessage("This group doesn't exists.");
        } else {
            Group group = new Group(param.get(1));
            permissionManager.getGroupManager().deleteGroup(group);
            channel.sendMessage("Group `" + group.getName() + "` has been removed from the database.");
        }
    }
}
