package com.gitlab.servertoolsbot.wikidevbot.commands.permissions.users;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.util.permissionmanager.User;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;

import java.util.stream.Collectors;

@Command(name = "listusers", aliases = { "users" }, description = "Lists all users in the database.")
public class ListUsersCommand extends CommonCommand {

    @Override
    @CommandParam(0)
    public void noArgumentsProvided() {
        channel.sendMessage("**These are all available users:**\n`" + permissionManager.getUserManager().getAllUsers().stream().map(User::getName).collect(Collectors.joining("`, `")) + "`");
    }
}
