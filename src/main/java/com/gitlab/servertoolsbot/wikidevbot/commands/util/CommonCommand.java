package com.gitlab.servertoolsbot.wikidevbot.commands.util;

import com.gitlab.servertoolsbot.util.commandmanager.BasicCommand;
import com.gitlab.servertoolsbot.util.messagemanager.MessageManager;
import com.gitlab.servertoolsbot.util.permissionmanager.PermissionManager;
import com.gitlab.servertoolsbot.wikidevbot.WikiDevBot;
import com.gitlab.servertoolsbot.wikidevbot.util.EmbedTemplate;
import com.gitlab.servertoolsbot.wikidevbot.util.util.PermissionUtil;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageAuthor;
import org.javacord.api.event.message.MessageCreateEvent;

public class CommonCommand extends BasicCommand {
    private WikiDevBot bot = WikiDevBot.getInstance();

    protected MessageCreateEvent event = bot.getEvent();
    protected TextChannel channel = event.getChannel();
    protected MessageAuthor author = event.getMessageAuthor();

    protected PermissionManager permissionManager = bot.getPermissionManager();
    protected MessageManager messageManager = bot.getMessageManager();

    protected PermissionUtil permissionUtil = new PermissionUtil(this.permissionManager);
    protected EmbedTemplate embed = new EmbedTemplate();


    @Override
    public void handle() {
        bot.incrementCommandExecutions();
    }

    @Override
    public void noPermission() {
        channel.sendMessage("Sorry, but you don't have the `" + getCommandAssertPermission() + "` permission and are not allowed to execute this command!");
    }

    @Override
    public Boolean performPermissionCheck(String permission) {
        return this.permissionUtil.performPermissionCheck(this.author.getIdAsString(), permission);
    }
}
