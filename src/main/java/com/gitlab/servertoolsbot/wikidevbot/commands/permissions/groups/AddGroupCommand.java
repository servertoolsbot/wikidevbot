package com.gitlab.servertoolsbot.wikidevbot.commands.permissions.groups;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.util.permissionmanager.Group;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;

import java.util.stream.Collectors;

@Command(name = "addgroup", description = "Add a group to the database.", usage = "<name>", assertPermission = "command.addgroup.execute")
public class AddGroupCommand extends CommonCommand {
    @Override
    @CommandParam(0)
    public void noArgumentsProvided() {
        channel.sendMessage("Sorry, but please give a group name");
    }

    @CommandParam(1)
    public void groupProvided() {
        if (permissionManager.getGroupManager().getAllGroups().stream().map(Group::getName).collect(Collectors.toList()).contains(param.get(1))) {
            channel.sendMessage("This group already exists.");
        } else {
            Group group = new Group(param.get(1));
            permissionManager.getGroupManager().saveGroup(group);
            channel.sendMessage("Group `" + group.getName() + "` has been saved to the database.");
        }
    }
}
