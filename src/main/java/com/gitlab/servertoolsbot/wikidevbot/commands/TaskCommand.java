package com.gitlab.servertoolsbot.wikidevbot.commands;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.util.phabricatorapi.PhabricatorApi;
import com.gitlab.servertoolsbot.util.phabricatorapi.data.model.Task;
import com.gitlab.servertoolsbot.util.phabricatorapi.data.model.TaskSearch;
import com.gitlab.servertoolsbot.util.phabricatorapi.data.model.status.Statuses;
import com.gitlab.servertoolsbot.wikidevbot.WikiDevBot;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;
import com.gitlab.servertoolsbot.wikidevbot.util.util.DateUtil;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Command(name = "task", usage = "<id>", description = "Shows information about tasks")
public class TaskCommand extends CommonCommand {
    @Override
    @CommandParam(1)
    public void noArgumentsProvided() {
        HashMap<String, Object> constraints = new HashMap<>();
        constraints.put("ids", Arrays.asList(Integer.valueOf(param.get(1))));

        TaskSearch search = new TaskSearch();
        search.setConstraints(new HashMap<String, Object>() {
            {
                put("ids", Arrays.asList(Integer.valueOf(param.get(1))));
            }
        });
        search.setLimit(1);

        Task t = search.perform();

        EmbedBuilder em = embed.common();
        em.setTitle("T"+ t.id + " " + t.name);
        em.setDescription(t.descriptionRaw + "\n");

        em.setColor(t.statusColor == null ? Color.GRAY : t.statusColor);
        em.addField(messageManager.getMessage("task.priority"), t.priorityName, true);
        em.addField(messageManager.getMessage("task.status"), t.statusName, true);
        em.addField(messageManager.getMessage("task.closed"), Statuses.get().closedStatuses.contains(t.statusValue) ? messageManager.getMessage("common.yes") : messageManager.getMessage("common.no"), true);

        em.addField(messageManager.getMessage("common.created"), DateUtil.formatDate(t.dateCreated), true);
        em.addField(messageManager.getMessage("common.modified"), DateUtil.formatDate(t.dateModified), true);
        if (t.dateClosed != 0) em.addField(messageManager.getMessage("task.closed"), DateUtil.formatDate(t.dateClosed), true);

        List<String> subUrls = new ArrayList<>();
        t.subscribers().forEach(sub -> subUrls.add("["+sub.username+"]("+ PhabricatorApi.getPapi().getUrlRaw() + "p/" + sub.username +")"));
        em.addField(messageManager.getMessage("task.subscribers"), String.join(", ", subUrls));

        channel.sendMessage(WikiDevBot.getInstance().getPapi().getUrlRaw() + "T" + t.id, em);
    }
}
