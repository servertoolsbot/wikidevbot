package com.gitlab.servertoolsbot.wikidevbot.commands.gerrit;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.wikidevbot.WikiDevBot;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;
import com.gitlab.servertoolsbot.wikidevbot.util.util.GerritUtil;
import com.google.gerrit.extensions.common.ChangeInfo;
import com.google.gerrit.extensions.restapi.RestApiException;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.util.ArrayList;
import java.util.List;

@Command(name = "changebyid", usage = "<id>", description = "Show information about a Gerrit change")
public class ChangeById extends CommonCommand {
    @Override
    @CommandParam(1)
    public void noArgumentsProvided() {
        EmbedBuilder em = embed.gray();
        String url = "";

        try {
            ChangeInfo change = WikiDevBot.getInstance().getGapi().changes().id(param.get(1)).get();
            url = WikiDevBot.getInstance().getGerritChangeUrl().replace("$%", String.valueOf(change._number));
            em.setTitle(change.subject);
            em.addField(messageManager.getMessage("gerrit.owner"), GerritUtil.buildGerritUserLink(change.owner), true);
            if (change.assignee != null) em.addField(messageManager.getMessage("gerrit.assignee"), GerritUtil.buildGerritUserLink(change.assignee), true);
            List<String> accountNames = new ArrayList<>();
            change.reviewers.forEach((state, accounts) -> accounts.forEach(account -> accountNames.add(GerritUtil.buildGerritUserLink(account))));
            em.addField(messageManager.getMessage("gerrit.reviewers"), String.join(", ", accountNames));
        } catch (RestApiException e) {
            e.printStackTrace();
        }

        channel.sendMessage("<" + url + ">", em);
    }
}
