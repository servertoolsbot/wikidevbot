package com.gitlab.servertoolsbot.wikidevbot.commands;

import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;
import com.gitlab.servertoolsbot.util.messagemanager.MessageReplacement;
import com.gitlab.servertoolsbot.util.phabricatorapi.PhabricatorApi;
import com.gitlab.servertoolsbot.util.phabricatorapi.data.model.Project;
import com.gitlab.servertoolsbot.util.phabricatorapi.data.model.ProjectSearch;
import com.gitlab.servertoolsbot.wikidevbot.commands.util.CommonCommand;
import com.gitlab.servertoolsbot.wikidevbot.util.util.MarkdownUtil;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Command(name = "project", usage = "<name>", description = "Search for a project using it's name.")
public class ProjectCommand extends CommonCommand {
    @Override
    @CommandParam(1)
    public void noArgumentsProvided() {
        EmbedBuilder em = embed.gray();

        HashMap<String, Object> constraints = new HashMap<>();
        constraints.put("query", "core:\"" + param.get(1) + "\"");

        ProjectSearch search = new ProjectSearch();
        search.setConstraints(new HashMap<String, Object>() {
            {
                put("query", "core:\"" + param.get(1) + "\"");
            }
        });
        search.setOrder(Arrays.asList("relevance"));
        search.setLimit(1);

        Project p = search.perform();

        em.setTitle(p.iconName + " " + p.name);

        em.addField(messageManager.getMessage("common.description"), MarkdownUtil.formatAll(p.description));
        if (p.members().isPresent()) {
            List<String> subUrls = new ArrayList<>();
            p.members().get().forEach(sub -> subUrls.add("["+sub.username+"]("+ PhabricatorApi.getPapi().getUrlRaw() + "p/" + sub.username +")"));
            em.addField(p.members().get().size() > 1 ? messageManager.getParsedMessage("project.member.plural", new MessageReplacement("count", String.valueOf(p.members().get().size()))) : messageManager.getParsedMessage("project.member.singular", new MessageReplacement("count", String.valueOf(p.members().get().size()))), String.join(", ", subUrls), true);
        }
        if (p.watchers().isPresent()) {
            List<String> subUrls = new ArrayList<>();
            p.watchers().get().forEach(sub -> subUrls.add("["+sub.username+"]("+ PhabricatorApi.getPapi().getUrlRaw() + "p/" + sub.username +")"));
            em.addField(p.watchers().get().size() > 1 ? messageManager.getParsedMessage("project.watcher.plural", new MessageReplacement("count", String.valueOf(p.watchers().get().size()))) : messageManager.getParsedMessage("project.watcher.singular", new MessageReplacement("count", String.valueOf(p.watchers().get().size()))), String.join(", ", subUrls), true);
        }

        channel.sendMessage(PhabricatorApi.getPapi().getUrlRaw() + "project/profile/" + p.id, em);
    }
}
