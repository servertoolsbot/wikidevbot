package com.gitlab.servertoolsbot.wikidevbot;

import org.javacord.api.util.logging.ExceptionLogger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Start {
    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Please give:" +
                    "\n1st argument: bot token" +
                    "\n2nd argument: database user" +
                    "\n3rd argument: database password" +
                    "\n4th argument: database host" +
                    "\n5th argument: database name");
            return;
        }

        setupLogging();

        WikiDevBot bot = new WikiDevBot();

        bot.start(args);
    }

    private static void setupLogging() {
        try {
            System.setProperty("java.util.logging.manager", org.apache.logging.log4j.jul.LogManager.class.getName());

            String log4jConfigurationFileProperty = System.getProperty("log4j.configurationFile");
            if (log4jConfigurationFileProperty != null) {
                Path log4jConfigurationFile = Paths.get(log4jConfigurationFileProperty);
                if (!Files.exists(log4jConfigurationFile)) {
                    try (InputStream fallbackLog4j2ConfigStream = Start.class.getResourceAsStream("/log4j2.xml")) {
                        Files.copy(fallbackLog4j2ConfigStream, log4jConfigurationFile);
                    }
                }
            }

            Thread.setDefaultUncaughtExceptionHandler(ExceptionLogger.getUncaughtExceptionHandler());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
